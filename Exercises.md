## 01
Given a list of numbers. For example:

    List<int\> numbers = new List<int\> {10, 14, 12, 1, 100, 3, 1, 10};

Write a program that counts how many instances of each number I have in the list.

## 02
Write a class that implements the following interface, the output of DisplayElements() should be done in the console. Write an example using this class.

    public  interface  IContainer<T>
    {
    	void AddElement(T element);
    	void RemoveElement(T element);
    	void DisplayElements();
    }

## 03
Write a class that contains an event *DoubleClickEvent* and a method *Click()*. Every second time I call the *Click()* method, the *DoubleClickEvent* should be raised.
Write a program using this class.