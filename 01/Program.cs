﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace _01
{
    class Program
    {
        static void Main(string[] args)
        {
            var numbers = new List<int> { 10, 14, 12, 1, 100, 3, 1, 10 };

            Console.WriteLine(string.Join(", ", numbers));
            Console.WriteLine("count instances");
            numbers.GroupBy(s => s).ToList().ForEach(g => Console.WriteLine("{1} {0}", g.FirstOrDefault(), g.Count()));
            

            Console.WriteLine("*******************");

            var linqMethod = from n in numbers
                             group n by n into g
                             select new { key = g.Key, instance = g.Count() };
            linqMethod.OrderByDescending(s => s.instance).ToList().ForEach(g => Console.WriteLine($"{g.key} {g.instance}"));

            Console.ReadKey();
        }
    }
}
